module.exports = function (gulp, plugins, config) {
    // Watch Task
    gulp.task('watch', function () {
        //Builds CSS
        plugins.watch(config.stylusSrcFolder + '/**/*.styl', function () {
            gulp.start('stylus');
        });

        // Build Pug
        plugins.watch(config.pugSrcFolder + '/**/*.pug', function () {
            gulp.start('pug');
        });

        // Inject CSS
        plugins.watch(config.stylusDestFolder + '/*.css', function () {
            gulp.start('inject');
        });

    });
};
