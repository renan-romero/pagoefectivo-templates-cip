module.exports = function (gulp, plugins, config) {
    gulp.task('default', function() {
        gulp.start('pug');
        gulp.start('stylus');
        // gulp.start('js-compile');
        gulp.start('inject');
        // gulp.start('imagemin');
        gulp.start('watch');
        gulp.start('browser-sync');
    });
};
